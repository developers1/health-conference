<?php
/**
 * Template Name: Sponsors page
 *
 * @package WordPress
 * @subpackage Tyler
 */

get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <div class="heading">
        <div class="container">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <?php the_content(); ?>
<?php endwhile; // end of the loop. ?>
    <script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>
<!--    <script src="js/jquery.isotope.js" type="text/javascript"></script>-->
<!--    <script src="js/sponsors.js" type="text/javascript"></script>-->
<?php get_footer();