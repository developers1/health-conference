<?php get_header(); ?>
<section id="sponsor-category-detail">
    <div class="container">
        <div class="row">
            <h1><?php echo single_cat_title('', false); ?></h1>
            <div class="category product-categories">
                <div class="category product-categories">
                    <a class="product-category" title="Click to return to categories"
                       href="http://www.therealtruthabouthealthconference.com/sponsors-categories/">Click to return
                        to categories</a>
                </div>
            </div>
            <div class="footer-sponsors">
                <ul>
                    <?php
                    $i = 0;
                    $j = 1;
                    while (have_posts()) : the_post(); ?>
                        <?php //get_template_part('content', get_post_type());
                        $tag_line = get_field('tagline', $spcat->ID);
                        $tag_line_color = get_field('tagline_font_color', $spcat->ID);
                        ?>
                        <li class="<?php echo "lirow" . $j; ?>">
                            <a href="<?php echo $spcat->guid; ?>"
                               title="<?php echo $spcat->post_title; ?>" target="_blank">
                                <img src="<?php echo get_the_post_thumbnail_url() ?>"
                                     alt="<?php the_title(); ?>"/>
                                <p class="sponsors-taglines"
                                   style="color:<?php echo $tag_line_color; ?>"><?php echo $tag_line; ?></p>
                                <p class="sponsors-website"><?php echo get_field('sponsor_link', $spcat->ID); ?></p>
                            </a>
                        </li>
                        <?php
                        $i++;
                        if ($i % 5 == 0) {
                            $j++;
                        }
                    endwhile;
                    //print_r($posts);
                    //   echo count($post);
                    //  echo $post->post_title;
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php //get_footer(); ?>
<script type="text/javascript">
    jQuery(window).on("load", function () {
        /*footer*/
        var totalsp = jQuery(".footer-sponsors ul li").length;
        var lastrow = Math.ceil(totalsp / 5);
        jQuery(".footer-sponsors ul li.lirow" + lastrow).each(function () {
            jQuery(this).addClass("last");
        });
    });
</script>