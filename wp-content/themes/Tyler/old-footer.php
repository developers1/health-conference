<?php
$ef_options = EF_Event_Options::get_theme_options();
?>
<section id="footer-sponsors">
    <div class="container">
        <div class="row">
            <h1>The Real Truth About Health 10 Day Conference<p>Is FREE due to the generosity of our sponsors.</p></h1>
            <h2>Please support them with your business<br/> and tell them that you appreciate this.</h2>
            <div class="category product-categories">
                <a class="product-category" title="Click to view by Product Category"
                   href="http://www.therealtruthabouthealthconference.com/sponsors-categories/" target="_blank">Click to
                    view by Product Category</a>
            </div>
            <div class="footer-sponsors">
                <?php
                $wp_footer_sponsors = new WP_Query(array(
                    'post_type' => 'sponsor',
                    'post_per_page' => -1,
                    'meta_key' => 'sponsor_order_no',
                    'orderby' => 'meta_value',
                    'order' => 'ASC'
                ));


                if ($wp_footer_sponsors->have_posts()):
                    ?>
                    <ul>
                        <?php
                        $i = 0;
                        $j = 1;
                        while ($wp_footer_sponsors->have_posts()):
                            $wp_footer_sponsors->the_post();
                            $taxonomy = 'sponsor-tier';
                            $terms = get_terms($taxonomy);
                            $term_list = wp_get_post_terms($post->ID, 'sponsor-tier', array("fields" => "names"));
                            $tag_line = get_field('tagline', $post->ID);
                            $tag_line_color = get_field('tagline_font_color', $post->ID);
                            ?>
                            <li class="<?php echo "lirow" . $j; ?>">
                                <a href="<?php echo get_field('sponsor_link', $post->ID) ?>"
                                   title="<?php the_title(); ?>" target="_blank">
                                    <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="<?php the_title(); ?>"/>
                                    <p class="sponsors-taglines"
                                       style="color:<?php echo $tag_line_color; ?>"><?php echo $tag_line; ?></p>
                                    <p class="sponsors-website"><?php echo get_field('sponsor_link', $post->ID); ?></p>
                                </a>
                            </li>
                            <?php
                            $i++;
                            if ($i % 5 == 0) {
                                $j++;
                            }
                        endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <h2 class="footer-title">Disclaimer:</h2>
        <p class="footer-text">The information and opinions expressed in this website and during our conferences include
            general discussions designed for educational purposes only. The information is not intended to substitute
            for medical advice or care. The opinions of our speakers and experts may not apply to you individually, and
            you should not use this information to diagnose or treat any health condition or illness without consulting
            your licensed physician first. Please consult a doctor with any questions or concerns you might have
            regarding your or your child's condition. TheRealTruthAboutHealth.com does not manufacture or distribute any
            commercial products, any reference herein to any specific commercial products, process, or service by trade
            name, trademark manufacturer, or otherwise, does not constitute or imply endorsement or recommendation by
            TheRealTruthAboutHealth.com.</p>
        <div class="row row-sm">
            <div class="footerlinks">
                <?php dynamic_sidebar('footer'); ?>
            </div>
        </div>
    </div>
    <div class="credits">
        <?php
        if (isset($ef_options['ef_footer_content'])) {
            echo stripslashes($ef_options['ef_footer_content']);
        }
        ?>
        <div class="footer-tyler-event">
            &copy;&nbsp;Copyright 2018 <strong><a href="http://therealtruthabouthealth.com/">The Real Truth About
                    Health</a></strong>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<!-- SCROLL UP BTN -->
<a href="#" id="scroll-up"><?php _e('UP', 'tyler'); ?></a>

<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- backdrop -->
<div id="backdrop"></div>


<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<!--
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1029944399;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1029944399/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<!-- Box Out Marketing Code for Lead Source Tracking Tag
<script src="https://bom.bz/scripts/1aeb26a0cffe9452" type="text/javascript"></script>-->

<script type="text/javascript">
    jQuery(window).on("load", function () {
        /*footer*/
        var totalsp = jQuery(".footer-sponsors ul li").length;
        var lastrow = Math.ceil(totalsp / 5);
        jQuery(".footer-sponsors ul li.lirow" + lastrow).each(function () {
            jQuery(this).addClass("last");
        });
    });
</script>
</body>
</html>
