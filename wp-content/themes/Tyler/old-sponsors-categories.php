<?php
/*
 * Template Name: Sponsors all categories
 * @package WordPress
 * @subpackage Tyler
*/
get_header();

if (have_posts()) :
    while (have_posts()) :
        the_post();
        ?>
        <div id="cs-content" class="cs-content allsponsors--catrgories-grid">
            <div id="x-section-1" class="x-section"
                 style="margin: 0px;padding: 45px 0px 0px; background-color: transparent;">
                <div class="x-container max width" style="margin: 0px auto;padding: 0px;">
                    <div class="x-column x-sm x-1-1" style="padding: 0px;">
                        <div class="x-text cs-ta-center innerpage-headings">
                            <div class="category product-categories">
                                <h1>The Real Truth About Health 10 Day Conference<p>Is FREE due to the generosity of our
                                        sponsors.</p></h1>
                                <h2>Please support them with your business<br/> and tell them that you appreciate this.</h2>
                                <div class="category product-categories">
                                    <a class="product-category" title="Click to view by Sponsor"
                                       href="http://www.therealtruthabouthealthconference.com/sponsors/">Click to view
                                        by Sponsor</a>
                                </div>
                            </div>
                        </div>
                        <hr class="x-gap" style="margin: 50px 0 0 0;">
                        <div class="x-raw-content">
                            <?php
                            $args = array(
                                'post_type' => 'sponsor',
                                'posts_per_page' => -1,
                                'orderby' => 'title',
                                'order' => 'ASC'
                            );
                            $posts_data = new WP_Query($args);
                            ?>
                            <ul class="sponsors-category-grid">
                                <?php
                                $i = 0;
                                $k = 0;
                                $j = 1;
                                $taxonomy = 'sponsor-tier';
                                $terms = get_terms($taxonomy);
                                foreach ($terms as $term) {
                                    $term_link = get_term_link($term);
                                    ?>
                                    <li class="sponsors-grid-item <?php echo $term->name . " " . ($i % 2 == 0 ? "black" : "orange")." "."lirow".$j; ?>">
                                        <a href="<?php echo $term_link; ?>" title="<?php echo $term->name ?>"
                                           target="_blank"><?php echo $term->name ?></a>
                                    </li>
                                    <?php
                                    $i++;
                                    $k++;
                                    if($k % 5 == 0){
                                        $j ++;
                                    }
                                    ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    endwhile;
endif;
//get_footer();
    ?>
<script type="text/javascript">
    jQuery(window).on("load", function(){
        /*categories*/
        var totalspcat = jQuery("ul.sponsors-category-grid li").length;
        var lastrowcat = Math.ceil(totalspcat / 5);
        jQuery("ul.sponsors-category-grid li.lirow"+lastrowcat).each(function(){
            jQuery(this).addClass("last");
        });
    });
</script>
