<?php
/*
 * Template Name: Sponsors all categories
 * @package WordPress
 * @subpackage Tyler
*/
get_header();

if (have_posts()) :
    while (have_posts()) :
        the_post();
        ?>
        <div id="cs-content" class="cs-content allsponsors--catrgories-grid" xmlns="http://www.w3.org/1999/html">
            <div id="x-section-1" class="x-section"
                 style="margin: 45px 0px 0px;padding: 0; background-color: transparent;">
                <div class="x-container max width" style="margin: 0px auto;padding: 0px;">
                    <div class="x-column x-sm x-1-1" style="padding: 0px;">
                        <div class="x-text cs-ta-center innerpage-headings">
                            <div class="category product-categories sponsors-headings">
                                <div class="sponsors-bottom-logos">
                                    <div class="greendrinks">
                                        <img
                                            src="<?php echo get_template_directory_uri(); ?>/images/greenjuice-logo.png"
                                            alt="Green drinks"/>
                                    </div>
                                    <div class="bottom-hippocrates">
                                        <h1>
                                            <a href="http://www.hippocrates.org/"
                                               title="Real Truth About Health Conference Sponsored by Hippocrates Health Institute"><img
                                                    src="<?php echo get_template_directory_uri(); ?>/images/bottom-hippocrates-logo.png"
                                                    alt="Real Truth About Health Conference Sponsored by Hippocrates Health Institute"/></a>
                                            <p>Is FREE due to the generosity<br>of our sponsors.</p></h1>
                                        <h2>Please support them with your<br>business and tell them that you<br>appreciate
                                            this.</h2>
                                    </div>
                                </div>
                                <div class="category product-categories">
                                    <a class="product-category" title="Click to view by Sponsor"
                                       href="http://www.therealtruthabouthealthconference.com/sponsors/">Click to view
                                        by Sponsor</a>
                                </div>
                            </div>
                        </div>
                        <div class="x-raw-content">
                            <?php
                            $args = array(
                                'post_type' => 'sponsor',
                                'posts_per_page' => -1,
                                'orderby' => 'title',
                                'order' => 'ASC'
                            );
                            $posts_data = new WP_Query($args);
                            ?>
                            <ul class="sponsors-category-grid">
                                <?php
                                $i = 0;
                                $k = 0;
                                $j = 1;
                                $taxonomy = 'sponsor-tier';
                                $terms = get_terms($taxonomy);
                                foreach ($terms as $term) {
                                    ;
                                    $term_link = get_term_link($term);
                                    $sponsor_tier_image = get_field('sponsor_thumbnail_image', $term->taxonomy . '_' . $term->term_id);
                                    ?>
                                    <li class="sponsors-grid-item <?php echo $term->name . " " . ($i % 2 == 0 ? "black" : "orange") . " " . "lirow" . $j; ?>">
                                        <a href="<?php echo $term_link; ?>" title="<?php echo $term->name ?>">
                                            <?php
                                            if ($sponsor_tier_image):
                                                ?><span class="category-logo"><img src="<?php echo $sponsor_tier_image ?>"></span>
                                                <?php
                                            endif;
                                            ?>
                                            <p class="textfill"><span><?php echo $term->name; ?></span></p>
                                        </a>
                                    </li>
                                    <?php
                                    $i++;
                                    $k++;
                                    if ($k % 5 == 0) {
                                        $j++;
                                    }
                                    ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    endwhile;
endif;
//get_footer();
?>
<script type="text/javascript"
        src="http://www.therealtruthabouthealthconference.com/wp-content/themes/Tyler/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.textfill.js"></script>
<footer>
    <div class="container">
        <h2 class="footer-title">Disclaimer:</h2>
        <p class="footer-text">The information and opinions expressed in this website and during our conferences include
            general discussions designed for educational purposes only. The information is not intended to substitute
            for medical advice or care. The opinions of our speakers and experts may not apply to you individually, and
            you should not use this information to diagnose or treat any health condition or illness without consulting
            your licensed physician first. Please consult a doctor with any questions or concerns you might have
            regarding your or your child's condition. TheRealTruthAboutHealth.com does not manufacture or distribute any
            commercial products, any reference herein to any specific commercial products, process, or service by trade
            name, trademark manufacturer, or otherwise, does not constitute or imply endorsement or recommendation by
            TheRealTruthAboutHealth.com.</p>
        <div class="row row-sm">
            <div class="footerlinks">
                <?php dynamic_sidebar('footer'); ?>
            </div>
        </div>
    </div>
    <div class="credits">
        <?php
        if (isset($ef_options['ef_footer_content'])) {
            echo stripslashes($ef_options['ef_footer_content']);
        }
        ?>
        <div class="footer-tyler-event">
            &copy;&nbsp;Copyright 2018 <strong><a href="http://therealtruthabouthealth.com/">The Real Truth About
                    Health</a></strong>
        </div>
    </div>
</footer>
<script type="text/javascript">
    jQuery(window).on("load", function () {
        var totalspcat = jQuery("ul.sponsors-category-grid li").length,
            lastrowcat = Math.ceil(totalspcat / 5),
            spMax = 0,
            spHeight = 0;
        jQuery("ul.sponsors-category-grid li.lirow" + lastrowcat).each(function () {
            jQuery(this).addClass("last");
        });
    });
</script>
