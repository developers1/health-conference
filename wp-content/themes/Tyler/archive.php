<?php get_header(); ?>
    <section id="sponsor-category-detail" data-text="<?php echo single_cat_title('', false); ?>">
        <div class="container">
            <div class="specific-category-details">
                <?php
                $obj = get_queried_object();
                $sponsor_tier_image = get_field('sponsor_banner_image', get_query_var('taxonomy') . '_' . $obj->term_id);
                ?>
                <?php
                if ($sponsor_tier_image):
                    ?>
                    <img src="<?php echo $sponsor_tier_image ?>">
                    <?php
                endif;
                ?>
                <p>Click below for our sponsors who sell</p>
                <h1><?php echo single_cat_title('', false); ?></h1>
            </div>
        </div>
        <span class="thick-border"></span>
        <div class="container">
            <div class="category product-categories">
                <div class="category product-categories">
                    <a class="product-category" title="Click to return to categories"
                       href="http://www.therealtruthabouthealthconference.com/sponsors-categories/">Click to return
                        to categories</a>
                </div>
            </div>
            <div class="footer-sponsors">
                <ul>
                    <?php
                    $i = 0;
                    $j = 1;
                    while (have_posts()) : the_post();
                        $tag_line = get_field('tagline', $spcat->ID);
                        $tag_line_color = get_field('tagline_font_color', $spcat->ID);
                        ?>
                        <li class="<?php echo "lirow" . $j; ?>">
                            <a href="http://<?php echo get_field('sponsor_link', $spcat->ID); ?>"
                               title="<?php the_title(); ?>" target="_blank">
                                <div class="sponsor-image">
                                    <img src="<?php echo get_the_post_thumbnail_url() ?>"
                                         alt="<?php the_title(); ?>"/>
                                </div>
                                <div class="sponsor-details">
                                    <p class="sponsors-taglines"
                                       style="color:<?php echo $tag_line_color; ?>">
                                        <span><?php echo $tag_line; ?></span></p>
                                    <p class="sponsors-website"><?php echo get_field('sponsor_link', $spcat->ID); ?></p>
                                </div>
                            </a>
                        </li>
                        <?php
                        $i++;
                        if ($i % 5 == 0) {
                            $j++;
                        }
                    endwhile;
                    ?>
                </ul>
            </div>
        </div>
    </section>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.textfill.js"></script>
    <script type="text/javascript">
        jQuery(window).on("load", function () {
            /*footer*/
            var totalsp = jQuery(".footer-sponsors ul li").length;
            var lastrow = Math.ceil(totalsp / 3);
            jQuery(".footer-sponsors ul li.lirow" + lastrow).each(function () {
                jQuery(this).addClass("last");
            });
            if (totalsp == 1) {
                jQuery(".footer-sponsors ul li").addClass("single-sponsor");
            }
        });
    </script>
    <script type="text/javascript"
            src="http://www.therealtruthabouthealthconference.com/wp-content/themes/Tyler/js/bootstrap.min.js"></script>
    <footer>
        <div class="container">
            <h2 class="footer-title">Disclaimer:</h2>
            <p class="footer-text">The information and opinions expressed in this website and during our conferences
                include
                general discussions designed for educational purposes only. The information is not intended to
                substitute
                for medical advice or care. The opinions of our speakers and experts may not apply to you individually,
                and
                you should not use this information to diagnose or treat any health condition or illness without
                consulting
                your licensed physician first. Please consult a doctor with any questions or concerns you might have
                regarding your or your child's condition. TheRealTruthAboutHealth.com does not manufacture or distribute
                any
                commercial products, any reference herein to any specific commercial products, process, or service by
                trade
                name, trademark manufacturer, or otherwise, does not constitute or imply endorsement or recommendation
                by
                TheRealTruthAboutHealth.com.</p>
            <div class="row row-sm">
                <div class="footerlinks">
                    <?php dynamic_sidebar('footer'); ?>
                </div>
            </div>
        </div>
        <div class="credits">
            <?php
            if (isset($ef_options['ef_footer_content'])) {
                echo stripslashes($ef_options['ef_footer_content']);
            }
            ?>
            <div class="footer-tyler-event">
                &copy;&nbsp;Copyright 2018 <strong><a href="http://therealtruthabouthealth.com/">The Real Truth About
                        Health</a></strong>
            </div>
        </div>
    </footer>
<?php //get_footer(); ?>