<?php get_header() ?>

<?php
//error_reporting(E_ALL);
//ini_set('display_errors',1);
// Get Theme Options
$ef_options = EF_Event_Options::get_theme_options();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();
    $single_session_fields = EF_Query_Manager::get_single_session_fields();

    extract($single_session_fields);
    ?>
    <div class="heading">
        <div class="container">
            <h1>
                <?php the_title() ?>
            </h1>

            <?php $prev_post = get_adjacent_post(true, '', true, 'category_name'); ?>
            <?php if (!empty($prev_post)): ?>
                <a href="<?php echo $prev_post->guid; ?>"><?php echo $prev_post->post_title; ?></a>
            <?php endif; ?>

            <?php $next_post = get_adjacent_post(true, '', false, 'category_name'); ?>
            <?php if (!empty($next_post)): ?>
                <a href="<?php echo $next_post->guid; ?>"><?php echo $next_post->post_title; ?></a>
            <?php endif; ?>

            <div class="nav">
                <?php //echo get_previous_post_link('%link', '<i class="icon-angle-left"></i>');
                tyler_previous_post_link_plus(array('order_by' => 'custom', 'meta_key' => 'session_date', 'format' => '%link', 'link' => '<i class="icon-angle-left"></i>'));
                ?>
                <?php if ($full_schedule_page && count($full_schedule_page) > 0) { ?>
                    <a href="<?php echo get_permalink($full_schedule_page[0]->ID); ?>"
                       title="<?php _e('All', 'tyler'); ?>"><i class="icon-th-large"></i></a>
                <?php } ?>
                <?php //echo get_next_post_link('%link', '<i class="icon-angle-right"></i>');
                tyler_next_post_link_plus(array('order_by' => 'custom', 'meta_key' => 'session_date', 'format' => '%link', 'link' => '<i class="icon-angle-right"></i>'));
                ?>
            </div>

        </div>
    </div>
    <div class="container">
        <p>
            <?php the_post_thumbnail('tyler-content'); ?>
        </p>

        <div class="row">
            <div class="col-md-12">
                <h1 class="session-title">
                    <?php the_title(); ?>
                </h1>
                <div class="session-details">
                    <div class="row">
                    <div class="session-info col-md-4 location"><?php _e('Location:', 'tyler'); ?>
                        <strong><?php echo(!empty($locations) ? $locations[0]->name : ''); ?></strong></div>
                    <div class="session-info col-md-4 date"><?php _e('Date:', 'tyler'); ?>
                        <strong><?php echo(!empty($date) ? date_i18n(get_option('date_format'), $date) : ''); ?></strong></div>
                    <div class="session-info col-md-4 time"><?php _e('Time:', 'tyler'); ?> <strong><?php echo $time; ?>
                            - <?php echo $end_time; ?></strong></div>
                    </div>
                    <?php the_content(); ?>
                </div>
                <!-- AddThis Button BEGIN -->
                <p class="clearfix">
                    <?php if (!empty($ef_options['ef_add_this_pubid'])) { ?>
                        <a class="addthis_button"
                           href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=<?php echo $ef_options['ef_add_this_pubid']; ?>">
                            <img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16"
                                 alt="<?php _e('Bookmark and Share', 'tyler'); ?>" style="border:0"/>
                        </a>
                    <?php } ?>
                </p>
                <!-- AddThis Button END -->
                <div style="padding:1em 0">

                    <?php
                    $full_schedule_page = get_posts(
                        array(
                            'post_type' => 'page',
                            'meta_key' => '_wp_page_template',
                            'meta_value' => 'schedule.php'
                        )
                    );
                    ?>

                    <?php foreach ($tracks as $track) { ?>
                        <a href="#" data-track="<?php echo $track->term_id; ?>"
                           class="single-session-link btn btn-primary" <?php if (!empty($track->color)) echo "style='background-color: $track->color;'"; ?>><?php echo $track->name; ?></a>
                    <?php } ?>
                </div>
            </div>

            <div class="col-md-12 sessions single">
                <div class="session">
                    <h2>Speakers</h2>
                    <div class="speakers-thumbs">
                        <?php
                            if (!empty($speakers_list)) {
                            foreach ($speakers_list as $speaker_id) {

                            ?>
                        <div class="row">
                            <div class="col-md-3 speaker-thumb-image">
                                <?php echo get_the_post_thumbnail($speaker_id, 'post-thumbnail', array('title' => get_the_title($speaker_id))); ?>
                            </div>
                            <div class="col-md-9 speaker-thumb-name">
                                <a href="<?php echo get_permalink($speaker_id); ?>"
                                   class="speaker<?php if (get_post_meta($speaker_id, 'speaker_keynote', true) == 1) echo ' featured'; ?>">
                                    <?php echo get_the_title($speaker_id); ?>
                                </a>
                                <?php
                                    $content_post = get_post($speaker_id);
                                    $content = $content_post->post_content;
                                    $content = apply_filters('the_content', $content);
                                    $content = str_replace(']]>', ']]&gt;', $content);
                                    $link=get_permalink($speaker_id);
                                ?>
                                <div class="auth-description">
                                    <?php echo wp_trim_words( $content, 100, '...' ); ?>
                                    <a href="<?php echo $link ?>" title="View bio" class="viewbio">View bio</a>
                                </div>
                            </div>
                        </div>

                    <?php }
                    } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if (!empty($registration_code)) { ?>
            <div style="margin-top: 40px">
                <h2 class="text-center"><?php echo $registration_title; ?></h2>

                <div style="padding: 1em">
                    <?php echo $registration_code; ?>
                </div>
                <p>
                    <?php echo get_post_meta(get_the_ID(), 'session_registration_text', true); ?>
                </p>
            </div>
        <?php } ?>
    </div>
    <?php
endwhile;
endif;
?>

<?php get_footer() ?>