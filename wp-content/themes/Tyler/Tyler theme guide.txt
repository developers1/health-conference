Please find Event Manager theme Guide at

http://www.eventmanagerblog.com/tyler-theme-guide

== Changelog ==

(09/26/2014)
* Version updated to 1.6.0
* Woocommerce integration. File(s) changed:
	- functions.php
	- css/layout.css
	- css/layout-mobile.css
	- woocommerce/ (+)
	- js/woocommerce.js (+)

(08/26/2014)
* Version updated to 1.5.4
* Fix issue with Registration Widget and Main Menu and Call to action buttons. File(s) changed:
	- index.php
	- event-framework/components/widgets/widget-registration.php
* Fix Schedule dropdown filters. File(s) changed:
	- schedule.php
	- event-framework/components/templates/schedule.php
	- css/layout.css
	- js/schedule.js

(08/21/2014)
* Version updated to 1.5.3
* Add "View Theme Documentation" Link to the Theme Options page. File(s) changed:
	- event-framework/inc/event-admin.php
* Add option to edit Registration Text in single session. File(s) changed:
	- event-framework/components/metaboxes/session.php
	- event-framework/helpers/cpt/query-manager.php
	- single-session.php
	
(07/28/2014)
* Version updated to 1.5.2
* Fix edge-case issue with registration button on Main Menu and Register Widget. File(s) changed:
	- index.php
	- event-framework/components/widgets/widget-registration.php
* Fix Media is not displaying if static homepage is set. File(s) changed: 
	- functions.php

(07/03/2014)
* Version updated to 1.5.1
* Fix Countdown issue. File(s) changed: 
	- event-framework/components/widgets/widget-timer.php

(06/30/2014)
* Version updated to 1.5
* Fix Hero background issue on mobile. File(s) changed:
	- css/layout-mobile.css 

(06/13/2014)
* Version updated to 1.4
* Remove Post info , share button and older post link in pages. File(s) changed:
	- page.php
* Fix "Sessions cannot have no speakers" issue. File(s) changed:
	- event-framework/components/metaboxes/session.php
* Fix "Menu in Mobile has problem". File(s) changed:
	- js/main.js

(06/04/2014)
* Version updated to 1.3
* Fix "Schedule not appearing" issue. File(s) changed:
	- event-framework/helpers/cpt/sessions.php
* Fix "Proper scrolling with arrows between sessions" - now the order should be by session date. File(s) changed:
	- functions.php
	- single-session.php 

(06/03/2014)
* Version updated to 1.2
* No HTML in registration widget. File(s) changed:
	- event-framework/components/widgets/widget-registration.php
* Add Widget Search filter for Widgets and Widget areas. File(s) changed:
	- event-framework/event-framework.php
	- event-framework/assets/js/search-widget.js


(05/28/2014)
* Version updated to 1.1
* Fix issue with Tyler Registration Widget register button. File(s) changed:
	- functions.php
	- index.php
	- event-framework/components/widgets/widget-registration.php
* Replace Speaker checkbox selector with AJAX for single Session. File(s) changed:
	- event-framework/components/metaboxes/session.php
* Fix issue with Tyler Latest News Widget if there is no "Blog" category. File(s) changed:
	- event-framework/components/widgets/widget-news.php
