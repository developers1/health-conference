<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <!--[if IE]>
    <meta name="X-UA-Compatible" content="IE=edge">        <![endif]-->        <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/ie8.css"/>        <![endif]-->
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-ie7.css"/>
    <style type="text/css">
        .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .input-group, .row, .content {
            box-sizing: border-box;
            behavior: url(<?php echo get_template_directory_uri();
            ?> /js/ boxsizing . htc);
        }
    </style>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/ie7.css"/>
    <![endif]-->
    <!-- HTML5 Shim, Respond.js and PIE.htc IE8 support of HTML5 elements, media queries and CSS3 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <style type="text/css">
        .speakers .speaker .speaker-inner, .speakers .photo img, .connect, .sessions .session .speaker img, .connect .links a:hover:before, .sessions .session .session-inner, .location .explore, .location .map, .articles article .image, .facebook .fb-event, .facebook .fb-view, .twitter .view, .twitter .tweet, .sidebar .widget_latest_comments li a, .sidebar h2, .articles article .image, .comments-area h2, .commentlist .comment .comment-content, .commentlist .comment .comment-content:after, .timecounter, input[type=text], textarea, .landing .box, h1 img.img-circle {
            behavior: url(<?php echo get_template_directory_uri();
            ?> /js/ pie / PIE . htc);
        }
    </style>
    <![endif]-->
    <?php wp_head(); ?>
    <!-- Facebook Pixel Code -->
    <script>!function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '919455068094335');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=919455068094335&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->    

<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107341159-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());
 
  gtag('config', 'UA-107341159-1');
</script>

</head>
<body <?php body_class(); ?>>
<header class="nav">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo esc_url(home_url()); ?>" id="head-logo">
                    <img src="<?php echo tyler_set_theme_logo(); ?>" alt="Logo <?php bloginfo('name'); ?>"
                         title="<?php bloginfo('name'); ?>"/>
                </a>
                <nav class="navbar" role="navigation">                <!-- mobile navigation -->
                    <div class="navbar-header visible-sm visible-xs">
                        <button type="button" class="btn btn-primary navbar-toggle" data-toggle="collapse"
                                data-target="#tyler-navigation"><span
                                class="sr-only"><?php _e('Toggle navigation', 'tyler'); ?></span> <i
                                class="icon-header"></i></button>
                    </div>                <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse text-fit" id="tyler-navigation">
                        <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_class' => 'pull-right', 'menu_id' => 'menu-primary')); ?>
                    </div>

                </nav>
            </div>
        </div>
    </div>
</header>
<?php
$cat=get_post_type();
if(($cat!="speaker") && (!is_page(2281)) && (!is_page(2163)) && (!is_page(4440)) && (!is_page(4692))){
    ?>
<section class="page-titles">
    <div class="container">
        <h1><?php the_title(); ?></h1>
    </div>
</section>
<script type="text/JavaScript">
    if(jQuery("body").hasClass("page-id-4692") == true){
        jQuery("body").addClass("home");
    }
</script>
<?php }?>
	