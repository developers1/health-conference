<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <?php $categories = get_the_category(); ?>
    <div class="heading">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <div class="container">
            <h1><?php the_title() ?></h1>
        </div>
    </div>
    <?php /*
<!--    <div class="container here">-->
<!--        <div class="row">-->
<!--            <div>--> */?>
                <?php the_post_thumbnail(null, array('class' => 'img-rounded')); ?>
                <?php the_content(); ?>
                <?php comments_template('', true); ?>
    <?php /*
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->*/    ?>
<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>