<?php
/**
 *Taxanomy Promotion Category
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://www.tob-dev.com/wp-content/themes/bretheon-child/style.css" rel="stylesheet">
    <link href="https://www.tob-dev.com/wp-content/themes/bretheon-child/hamilton_new_css/style.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <title>Promotions</title>
    <?php wp_head(); ?>
</head>
<?php get_header("promotion"); ?>
<body id="himlton_princess_pages_body"
      class="page-template page-template-hamilton_princess_new page-template-hamilton_princess_new-php page page-id-5770 page-child parent-pageid-6291  with_aside aside_right layout-boxed footer-separate wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
<div class="wrapper">
    <div class="contanier">
        <div class="row">
            <div class="col-md-12">
                <?php
                $term = get_queried_object();
                $term_id = $term->term_id;
                $taxonomy_name = $term->taxonomy;
                the_field('category_description', $term->taxonomy . '_' . $term->term_id);
                ?>
                <!--<h3>Why do we use it?</h3>
                <p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry</strong><br>
                    It is a long established fact that a reader will be distracted by the readable content of a page
                    when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                    distribution of letters, as opposed to using 'Content here, content here', making it look like
                    readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their
                    default model text, and a search for 'lorem ipsum' will uncover many web sites still in their
                    infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose
                    (injected humour and the like).
                </p>-->
            </div>
        </div>
        <?php
        $term_children = get_term_children($term_id, $taxonomy_name);
        if (!empty($term_children)) {
            foreach ($term_children as $child) {
                $term = get_term_by('id', $child, $taxonomy_name);
                //if ($term->parent == 196) { ?>
                <?php if ($i % 2 == 0) { ?>
                    <div class="row">
                <?php } ?>
                <div class="col-md-6">
                    <div class="category-block medium-cat">
                        <figure><img
                                    src="<?php the_field('category_image', $term->taxonomy . '_' . $term->term_id); ?>"
                                    alt="category01">
                            <figcaption class="overlay-info">
                                <h4><a href="<?php echo get_term_link($term) ?>"><?php echo $term->name; ?></a></h4>
                                <!--<a href="<?php /*get_page_link( $portfolio_page_id ). $translate['all'] */ ?></a>-->
                                <div class="text-ellipsis"><?php echo $term->name; ?></div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <?php
                $i++;
                ?>
                <?php if ($i % 2 == 0) { ?>  </div> <?php } ?>
                <?php //}
            }
        } else {
        $posts_array = get_posts(
            array(
                'posts_per_page' => mfn_opts_get('promotions-posts', 6),
                'post_type' => 'promotions',
                'tax_query' => array(
                    array(
                        'taxonomy' => $taxonomy_name,
                        'field' => 'term_id',
                        'terms' => $term_id,
                    )
                )
            )
        );
        ?>
        <?php// while (have_rows('all_hotel_gallery', $term->taxonomy . '_' . $term->term_id)): the_row(); ?>
        <div class="row">
            <div class="col-md-9">
                <?php
                $gallery = get_field('gallery', $term->taxonomy . '_' . $term->term_id);
                // print_r($gallery);exit;
                // putUniteGallery("hotelA1Delux");
                echo do_shortcode($gallery);
                // echo do_shortcode("[unitegallery hotelA1Delux]");
                //echo "out";exit;[unitegallery Cambridge]
                ?>
            </div>
            <div class="col-md-3">
                <div class="row imgpop">
                    <div class="col-xs-12 col-md-12">
                        <div class="hotels-gallery small-cat">
                            <figure>
                                <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg"
                                   class="gallery-slider-icon">
                                    <img alt="category01"
                                         src="<?php the_field('single_image', $term->taxonomy . '_' . $term->term_id); ?>">
                                </a>
                            </figure>
                            <div class="container text-center">
                                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div id="carousel-example-generic" class="carousel slide"
                                                 data-ride="carousel">
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner">
                                                    <?php
                                                    $i = 1;
                                                    foreach ($posts_array as $post) {
                                                        ?>
                                                        <div class="item <?php echo ($i == 1) ? 'active' : ''; ?>">
                                                            <img class="img-responsive"
                                                                 src="<?php the_post_thumbnail_url('full'); ?>"
                                                                 alt="...">
                                                        </div>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>
                                                </div>
                                                <!-- Controls -->
                                                <a class="left carousel-control" href="#carousel-example-generic"
                                                   role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                                <a class="right carousel-control" href="#carousel-example-generic"
                                                   role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row imgpop">
                    <div class="col-xs-12 col-md-12">
                        <div class="hotels-video small-cat">
                            <figure>
                                <?php $video_url = get_field('video', $term->taxonomy . '_' . $term->term_id);
                                $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
                                $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';
                                if (preg_match($longUrlRegex, $video_url, $matches)) {
                                    $youtube_id = $matches[count($matches) - 1];
                                }
                                if (preg_match($shortUrlRegex, $video_url, $matches)) {
                                    $youtube_id = $matches[count($matches) - 1];
                                }
                                $short_code = str_replace("embed_code", $youtube_id, "[video_lightbox_youtube video_id=\"embed_code\" width=\"640\" height=\"480\" anchor=\"https://img.youtube.com/vi/embed_code/hqdefault.jpg\"]");
                                ?>
                                <?php echo do_shortcode($short_code); ?>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php //endwhile; ?>
    <div class="row">
        <div class="col-md-12 promotions-hotel-list-block">
            <div class="clearfix gap-30">&nbsp;</div>
            <h3><?php echo $term->name; ?> PROMOTIONS</h3>
            <?php foreach ($posts_array as $post) { ?>
                <div class="promotions-hotel-list">
                    <figure class="hotel-pic"><img src="<?php the_post_thumbnail_url('full'); ?>" alt="">
                    </figure>
                    <h4>
                        <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title ?></a>
                    </h4>
                    <?php
                    $content_post = get_post($post->ID);
                    $content = $content_post->post_content;
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    ?>
                    <div class="promotions-hotel-desc">
                        <?php echo wp_trim_words( $content, 20, '...' ); ?>
                    </div>
                    <div class="booked-time"><strong>Must be booked in this time frame : </strong>
                        <em><?php
                            echo date("d/m/Y", strtotime(get_field('booking_start_time'))); ?>
                            to <?php echo date("d/m/Y", strtotime(get_field('booking_end_time'))); ?></em>
                    </div>
                    <a class="book-btn btn-orange pull-right" href="<?php the_field('booking_url'); ?>">Book</a>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php }
    //   }
    ?>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
</script>
<?php get_footer("promotion"); ?>
</html>