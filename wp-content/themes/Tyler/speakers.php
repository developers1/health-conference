<?php
/*
 * Template Name: Speakers
 *
 * @package WordPress
 * @subpackage Tyler
 */
?>
<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <div class="heading">
        <div class="container">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="container">
<p>
                    </p>
                    <hr>
            <h2></h2>
                            <div class="speakers">
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/hope-bohanec/">
                                <span class="photo">
                                    <img width="331" height="220" title="HOPE BOHANEC" alt="Hope_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/01/Hope_profile4-282x188.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">HOPE BOHANEC</span></span></span>
                                <span class="description">The Humane Hoax: Cage-free? Free-range? Grass-fed? What do the labels really mean for animals and the planet?
</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/dr-helen-caldicott/">
                                <span class="photo">
                                    <img width="212" height="145" title="DR. HELEN CALDICOTT" alt="Helen_Profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/Helen_Profile.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">HELEN CALDICOTT M.D.</span></span></span>
                                <span class="description">Crisis Without End: The Medical and Ecological Consequences of the Fukushima Nuclear Catastrophe</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>

                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/anna-maria-clement-phdnmdln/">
                                <span class="photo">
                                    <img width="212" height="145" title="ANNA MARIA CLEMENT, PHD,NMD,LN" alt="anna_Profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/anna_Profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">ANNA MARIA CLEMENT, Ph.D., L.N.</span></span></span>
                                <span class="description">Accessing the Lifeforce Within You Through Raw and Living Foods</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/brian-clement-phdnmdln/">
                                <span class="photo">
                                    <img width="212" height="145" title="BRIAN CLEMENT, PHD,NMD,LN" alt="brian_Profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/brian_Profile3.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">BRIAN CLEMENT, Ph.D., L.N.</span></span></span>
                                <span class="description">How to Prevent Disease and Premature Aging</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/gabriel-cousens-m-d/">
                                <span class="photo">
                                    <img width="212" height="145" title="GABRIEL COUSENS, M.D." alt="gabriel_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/gabriel_profile.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">GABRIEL COUSENS, M.D.</span></span></span>
                                <span class="description">Cousens' 3 week Diabetes Recovery Program is the Most Successful Antidiabetes Program in the World</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/hans-diehl-drhsc-mph-facn/">
                                <span class="photo">
                                    <img width="212" height="145" title="HANS DIEHL, DrHSc, MPH, FACN" alt="hans_Profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/hans_Profile.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">HANS DIEHL, DrHSc, MPH, FACN</span></span></span>
                                <span class="description">How to Eat More and Live Longer and Better</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/john-englander/">
                                <span class="photo">
                                    <img width="212" height="145" title="JOHN ENGLANDER" alt="johnenglander_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/11/johnenglander_profile.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">JOHN ENGLANDER</span></span></span>
                                <span class="description">Rising Sea Level and the Coming Coastal Crisis</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/joel-fuhrman-m-d/">
                                <span class="photo">
                                    <img width="212" height="145" title="JOEL FUHRMAN M.D." alt="joelfurman_Profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/joelfurman_Profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">JOEL FUHRMAN M.D.</span></span></span>
                                <span class="description">The End of Dieting: The Amazing Nutrient Rich Program for Fast and Sustained Weight Loss</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/robert-glennon/">
                                <span class="photo">
                                    <img width="212" height="145" title="ROBERT GLENNON" alt="glennon_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/glennon_profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">ROBERT GLENNON</span></span></span>
                                <span class="description">America's Water Crisis and What To Do About It</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/mckay-jenkins/">
                                <span class="photo">
                                    <img width="212" height="145" title="MCKAY JENKINS" alt="jenkins_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/11/jenkins_profile2.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">MCKAY JENKINS, Ph.D.</span></span></span>
                                <span class="description">The Threats—Biological and Environmental—that Chemicals Now Present In Our Daily Lives</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/irving-kirsch/">
                                <span class="photo">
                                    <img width="212" height="145" title="IRVING KIRSCH" alt="Irvine_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/Irvine_profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">IRVING KIRSCH, Ph.D.</span></span></span>
                                <span class="description">Exploding the Antidepressant Myth</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/david-montgomery/">
                                <span class="photo">
                                    <img width="212" height="145" title="DAVID MONTGOMERY" alt="montgomery_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/montgomery_profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">DAVID MONTGOMERY</span></span></span>
                                <span class="description">Soil is Humanity's Most Essential Natural Resource and Essentially Linked to Modern Civilization's Survival</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                      <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/margaret-paul-ph-d/">
                                <span class="photo">
                                    <img width="212" height="145" title="MARGARET PAUL, PH.D." alt="margaret_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/margaret_profile-copy.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">MARGARET PAUL, Ph.D.</span></span></span>
                                <span class="description">Becoming a Loving Adult to Your Inner Child</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/david-simon/">
                                <span class="photo">
                                    <img width="212" height="145" title="DAVID SIMON" alt="davidsimon_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/davidsimon_profile2.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">DAVID ROBINSON SIMON, J.D.</span></span></span>
                                <span class="description">Adding Up the Huge “Externalized” Costs that the Animal Food System Imposes on Taxpayers, Animals and the Environment</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/michele-simon/">
                                <span class="photo">
                                    <img width="212" height="145" title="MICHELE SIMON" alt="michele_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/michele_profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">MICHELE SIMON, J.D., MPH</span></span></span>
                                <span class="description">How the Food Industry Undermines our Health and How to Fight Back</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/melanie-warner/">
                                <span class="photo">
                                    <img width="212" height="145" title="MELANIE WARNER" alt="Melanie_profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/Melanie_profile1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">MELANIE WARNER</span></span></span>
                                <span class="description">How Processed Food Took Over the American Meal</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                                        	                        <div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/robert-whitaker/">
                                <span class="photo">
                                    <img width="212" height="145" title="ROBERT WHITAKER" alt="robertwhit_Profile" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2014/10/robertwhit_Profile.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">ROBERT WHITAKER</span></span></span>
                                <span class="description">Do Psychiatric Medications Fix “Chemical Imbalances” in the Brain, or Do They, In Fact, Create Them?</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/nelson-campbell/">
                                <span class="photo">
                                    <img width="212" height="145" title="NELSON CAMPBELL" alt="NelsonCampbell_Profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/NelsonCampbell_Profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">NELSON CAMPBELL</span></span></span>
                                <span class="description">Film Showing of Pure Plant Nation</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                       	<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/gwen-olsen/">
                                <span class="photo">
                                    <img width="212" height="145" title=“GWEN OLSEN” alt="Gwen_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/Gwen_profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">GWEN OLSEN</span></span></span>
                                <span class="description">Confessions of an Rx Drug Pusher</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/gary-null-ph-d/">
                                <span class="photo">
                                    <img width="212" height="145" title=“GARY NULL PH.D.“ alt="GaryNull_Profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/GaryNull_Profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">GARY NULL Ph.D.</span></span></span>
                                <span class="description">The Healthy Vegetarian: Healing Yourself, Healing Our Planet</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/dr-will-tuttle-ph-d/">
                                <span class="photo">
                                    <img width="212" height="145" title=“DR. WILL TUTTLE PH.D.“ alt="willtuttle_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/willtuttle_profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">DR. WILL TUTTLE Ph.D.</span></span></span>
                                <span class="description">The World Peace Diet, Circles of Compassion, and Conscious Eating</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/dr-robert-d-morris-ph-d/">
                                <span class="photo">
                                    <img width="212" height="145" title=“DR. ROBERT D. MORRIS PH.D.“ alt="RobertMorris_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/RobertMorris_profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">DR. ROBERT D. MORRIS, Ph.D.</span></span></span>
                                <span class="description">The Risk of the Invisible: from Cholera to Cell Phones</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/claire-hope-cummings/">
                                <span class="photo">
                                    <img width="212" height="145" title=“CLAIRE HOPE CUMMINGS” alt="clairecummings_Profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/claireMontgomery_profile-book1.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">CLAIRE HOPE CUMMINGS, M.A., J.D.,</span></span></span>
                                <span class="description">The Truth about Seeds - from the Garden of Eden to GMOs</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/brenda-davis/">
                                <span class="photo">
                                    <img width="212" height="145" title=“BRENDA DAVIS” alt="BrendaDavis_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/BrendaDavis_profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">BRENDA DAVIS, J.D.</span></span></span>
                                <span class="description">Becoming Vegan</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/seth-b-darling/">
                                <span class="photo">
                                    <img width="212" height="145" title=“SETH B. DARLING“ alt="SethDarling_Profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/SethDarling_Profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">SETH B. DARLING, Ph.D.</span></span></span>
                                <span class="description">Climate disruption: What we can do</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
                        						<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/jeffrey-m-smith/">
                                <span class="photo">
                                    <img width="212" height="145" title=“JEFFREY M. SMITH“ alt="JefferySmith_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/JefferySmith_profile-book.jpg">                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">JEFFREY SMITH</span></span></span>
                                <span class="description">Seeds of Deception: Exposing Industry and Government Lies about the Safety of the Genetically Engineered Foods You’re Eating</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/roger-l-greenlaw-m-d/">
                                <span class="photo">
                                    <img title="ROGER L. GREENLAW, M.D." alt="JefferySmith_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/04/greenlaw_profilePage.jpg"/>                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">ROGER L. GREENLAW, M.D.</span></span></span>
                                <span class="description">Medical Director of the Swedish American Center for Complementary Medicine</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>
<div class="speaker ">
                            <a class="speaker-inner" href="http://therealtruthabouthealthconference.com/speakers/steven-drucker-jd/">
                                <span class="photo">
                                    <img title=“STEVEN DRUCKER, JD” alt=“StevenDrucker_profile+book" class="attachment-tyler-speaker wp-post-image" src="http://therealtruthabouthealthconference.com/wp-content/uploads/2015/05/StevenDrucker_profile-book.jpg"/>                                </span>
                                <span class="name"><span class="text-fit"><span style="display:block" class="text-fit-inner">STEVEN DRUCKER, J.D.</span></span></span>
                                <span class="description">How the venture to genetically engineer our food has subverted science, corrupted government, and systematically deceived the public.</span>
                                <span class="view">
                                    View profile <i class="icon-angle-right"></i>
                                </span>
                            </a>
                        </div>

                                    </div>
                                <hr>
            <h2></h2>
                                <hr>
            <h2></h2>
                        </div>

        <p>

        </p>

<?php endwhile; // end of the loop. ?>

<?php get_footer() ?>