<?php
/*
 * Template Name: Company sponsors Template
 * @package WordPress
 * @subpackage Tyler
*/

get_header();
?>
<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();
        ?>
        <div class="heading">
            <div class="container">
                <h1>
                    <?php the_title() ?>
                </h1>
            </div>
        </div>
        <div id="cs-content" class="cs-content allsponsors-grid">
            <div id="x-section-1" class="x-section"
                 style="margin: 0px;padding: 45px 0px 0px; background-color: transparent;">
                <div class="x-container max width" style="margin: 0px auto;padding: 0px;">
                    <div class="x-column x-sm x-1-1" style="padding: 0px;">
                        <div class="overlay">
                            <div id="loading-img"></div>
                        </div>
                        <div class="x-text cs-ta-center innerpage-headings">
                            <div class="category product-categories">
                                <h1>The Real Truth About Health 10 Day Conference<p>Is FREE due to the generosity of our
                                        sponsors.</p></h1>
                                <h2>Please support them with your business<br/> and tell them that you appreciate this.
                                </h2>
                                <div class="category product-categories">
                                    <a class="product-category" title="Click to view by Product Category"
                                       href="http://www.therealtruthabouthealthconference.com/sponsors-categories/">Click
                                        to view by Product Category</a>
                                </div>
                            </div>
                        </div>
                        <hr class="x-gap" style="margin: 50px 0 0 0;">
                        <div class="x-raw-content">
                            <div class="footer-sponsors">
                                <?php
                                $wp_footer_sponsors = new WP_Query(array(
                                    'post_type' => 'sponsor',
                                    'post_per_page' => -1,
                                    'meta_key' => 'sponsor_order_no',
                                    'orderby' => 'meta_value',
                                    'order' => 'ASC'
                                ));
                                if ($wp_footer_sponsors->have_posts()):
                                    ?>
                                    <ul>
                                        <?php
                                        $i = 0;
                                        $j = 1;
                                        while ($wp_footer_sponsors->have_posts()):
                                            $wp_footer_sponsors->the_post();
                                            $taxonomy = 'sponsor-tier';
                                            $terms = get_terms($taxonomy);
                                            $term_list = wp_get_post_terms($post->ID, 'sponsor-tier', array("fields" => "names"));
                                            $tag_line = get_field('tagline', $post->ID);
                                            $tag_line_color = get_field('tagline_font_color', $post->ID);
                                            ?>
                                            <li class="<?php echo "lirow" . $j; ?>">
                                                <a href="<?php echo get_field('sponsor_link', $post->ID) ?>"
                                                   title="<?php the_title(); ?>" target="_blank">
                                                    <img src="<?php echo get_the_post_thumbnail_url() ?>"
                                                         alt="<?php the_title(); ?>"/>
                                                    <p class="sponsors-taglines"
                                                       style="color:<?php echo $tag_line_color; ?>"><?php echo $tag_line; ?></p>
                                                    <p class="sponsors-website"><?php echo get_field('sponsor_link', $post->ID); ?></p>
                                                </a>
                                            </li>
                                            <?php
                                            $i++;
                                            if ($i % 5 == 0) {
                                                $j++;
                                            }
                                        endwhile; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    endwhile;
endif;
//get_footer();
?>
<script type="text/javascript">
    jQuery(window).on("load", function () {
        /*footer*/
        var totalsp = jQuery(".footer-sponsors ul li").length;
        var lastrow = Math.ceil(totalsp / 5);
        jQuery(".footer-sponsors ul li.lirow" + lastrow).each(function () {
            jQuery(this).addClass("last");
        });
    });
</script>

